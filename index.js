// Tạo mảng
var numberArray = [];
document
  .getElementById("txt-btn-them-so")
  .addEventListener("click", function () {
    var numberValue = document.getElementById("txt-input-number").value * 1;
    numberArray.push(numberValue);
    document.getElementById("txt-input-number").value = "";
    document.getElementById("txt-ket-qua").innerText = numberArray;
  });
// Tạo mảng End

// Tổng số dương
document
  .getElementById("txt-btn-tinh-tong-so-duong")
  .addEventListener("click", function () {
    var sum = 0;
    for (var index = 0; index < numberArray.length; index++) {
      if (numberArray[index] >= 0) {
        sum = sum + numberArray[index];
      }
    }
    document.getElementById("txt-ket-qua-tong-so-duong").innerHTML = `<div>
    Tổng số dương là: ${sum}
    </div>`;
  });

// tổng số dương End

// Đếm số dương
document
  .getElementById("txt-btn-dem-so-duong")
  .addEventListener("click", function () {
    var count = 0;
    for (var index = 0; index < numberArray.length; index++) {
      if (numberArray[index] > 0) {
        count++;
      }
    }
    document.getElementById("txt-ket-qua-dem-so-duong").innerHTML = `<div>
    Số dương: ${count}
    </div>`;
  });
// Đếm số dương End

// Tìm số nhỏ nhất
document
  .getElementById("txt-btn-tim-so-nho-nhat")
  .addEventListener("click", function () {
    min = numberArray[0];
    for (var index = 0; index < numberArray.length; index++) {
      if (numberArray[index] < min) {
        min = numberArray[index];
      }
    }
    document.getElementById("txt-ket-qua-tim-so-nho-nhat").innerHTML = `<div>
    Số nhỏ nhất là: ${min}
    </div>`;
  });
// Tìm số nhỏ nhất End

// Tìm số dương nhỏ nhất
document
  .getElementById("txt-btn-tim-so-duong-nho-nhat")
  .addEventListener("click", function () {
    var integerNumberValue = [];
    for (var index = 0; index < numberArray.length; index++) {
      if (numberArray[index] > 0) {
        integerNumberValue.push(numberArray[index]);
      }
    }
    if (integerNumberValue.length == 0) {
      document.getElementById(
        "txt-ket-qua-tim-so-duong-nho-nhat"
      ).innerHTML = `<div>
    Không có số dương trong mảng
    </div>`;
    } else {
      var minInteger = integerNumberValue[0];
      for (var index = 0; index < integerNumberValue.length; index++) {
        if (integerNumberValue[index] < minInteger) {
          minInteger = integerNumberValue[index];
        }
      }
      document.getElementById(
        "txt-ket-qua-tim-so-duong-nho-nhat"
      ).innerHTML = `<div>
    Số dương nhỏ nhất là: ${minInteger}
    </div>`;
    }
  });
// Tìm số dương nhỏ nhất End

// Tìm số chẵn cuối cùng
document
  .getElementById("txt-btn-tim-so-chan-cuoi-cung")
  .addEventListener("click", function () {
    var lastNumber = 0;
    for (var index = 0; index < numberArray.length; index++) {
      if (numberArray[index] % 2 == 0) {
        lastNumber = numberArray[index];
      }
    }
    document.getElementById(
      "txt-ket-qua-tim-so-chan-cuoi-cung"
    ).innerHTML = `<div>
Số chẵn cuối cùng là: ${lastNumber}
</div>`;
  });
// Tìm số chẵn cuối cùng End

// Đổi chỗ
document
  .getElementById("txt-btn-doi-cho")
  .addEventListener("click", function () {
    var index1Value = document.getElementById("txt-input-index1").value * 1;
    var index2Value = document.getElementById("txt-input-index2").value * 1;
    var current = 0;
    var numberArraySwap = 0;

    current = numberArray[index1Value];
    numberArray[index1Value] = numberArray[index2Value];
    numberArray[index2Value] = current;

    document.getElementById("txt-ket-qua-doi-cho").innerHTML = `<div>
Mảng sau khi đổi chỗ là: ${numberArray}
</div>`;
  });
// Đổi chỗ End

// Sắp xếp
document
  .getElementById("txt-btn-sap-xep")
  .addEventListener("click", function () {
    numberArray.sort((a, b) => a - b);
    document.getElementById("txt-ket-qua-sap-xep").innerHTML = `<div>
Mảng sau khi sắp xếp là: ${numberArray}
</div>`;
  });
// Sắp xếp End

// Đếm số nguyên
document
  .getElementById("txt-btn-dem-so-nguyen")
  .addEventListener("click", function () {
    var count = 0;
    for (var index = 0; index < numberArray.length; index++) {
      if (Number.isInteger(numberArray[index]) == true) {
        count++;
      }
    }
    document.getElementById("txt-ket-qua-dem-so-nguyen").innerHTML = `<div>
Số nguyên: ${count}
</div>`;
  });
// Đếm số nguyên End

// So sánh âm dương
document
  .getElementById("txt-btn-so-sanh")
  .addEventListener("click", function () {
    countDuong = 0;
    countAm = 0;
    for (var index = 0; index < numberArray.length; index++) {
      if (numberArray[index] > 0) {
        countDuong++;
      } else {
        countAm++;
      }
    }
    if (countAm < countDuong) {
      document.getElementById("txt-ket-qua-so-sanh").innerHTML = `<div>
      Số âm < số dương
      </div>`;
    } else if (countAm > countDuong) {
      document.getElementById("txt-ket-qua-so-sanh").innerHTML = `<div>
      Số âm > số dương
      </div>`;
    } else {
      document.getElementById("txt-ket-qua-so-sanh").innerHTML = `<div>
      Số âm = số dương
      </div>`;
    }
  });
// So sánh âm dương End
